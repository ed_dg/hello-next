import Layout from '../comps/MyLayout'

export default function About() {
    return (
      <Layout>
        <p>Rockstar architects, business analysts, project managers, all at play and devoted for what we consider the most important phase of the flow. Using the best tools and intelligence on estimations, plan, design and prototyping. </p>
      </Layout>
    );
  }